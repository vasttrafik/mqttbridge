DOCKER_DIR=		docker
DOCKER_IMAGE=		builder
CONTAINER=		bob_the_builder

VERSION=		1.0.3
PATCHLEVEL=		1
PACKAGE_VERSION=	$(VERSION)-$(PATCHLEVEL)
PACKAGE_NAME=		mqttbridge-$(PACKAGE_VERSION)_armhf


all:

deb:
	-mkdir $(DOCKER_DIR)
	cp Dockerfile $(DOCKER_DIR)
	cp -r mqttbridge $(DOCKER_DIR)
	cp -r debian $(DOCKER_DIR)
	docker build -t $(DOCKER_IMAGE) -f $(DOCKER_DIR)/Dockerfile $(DOCKER_DIR)
	docker run -it --name $(CONTAINER) $(DOCKER_IMAGE) sh package.sh -v $(VERSION) -p $(PATCHLEVEL)
	docker cp $(CONTAINER):/usr/src/debian/$(PACKAGE_NAME).deb .
	docker rm $(CONTAINER)

clean:
	rm -fr $(DOCKER_DIR) *.deb
