#!/bin/bash
#
# package mqttbridge

PYTHON=${PYTHON:-python2.7}
ARCH=`dpkg --print-architecture`
WHEEL=mqttbridge

VERSION=0.0.0
PATCHLEVEL=0

while getopts "v:p:" opt; do
	case $opt in
		v)
			# debian package version
			VERSION=$OPTARG
			;;
		p)
			# debian patchlevel version
			PATCHLEVEL=$OPTARG
			;;
	esac
done

CONTROL_TEMPLATE=`dirname $0`/${WHEEL}.control
SERVICE_TEMPLATE=`dirname $0`/${WHEEL}.service
PACKAGE_NAME=`echo $WHEEL | sed s/_/-/`
SERVICE_NAME=$PACKAGE_NAME
VENV=/opt/`echo $WHEEL | sed s/_/-/`

DESTDIR=/var/tmp/${PACKAGE_NAME}-${VERSION}-${PATCHLEVEL}_${ARCH}
PACKAGE=${DESTDIR}.deb

rm -fr $DESTDIR
mkdir -p $DESTDIR/opt
mv $VENV $DESTDIR/opt

mkdir -p $DESTDIR/lib/systemd/system
install -m 444 $SERVICE_TEMPLATE $DESTDIR/lib/systemd/system/${SERVICE_NAME}.service

mkdir -p $DESTDIR/DEBIAN
sed \
	-e "s/_ARCH_/${ARCH}/" \
	-e "s/_VERSION_/${VERSION}-${PATCHLEVEL}/" \
	< $CONTROL_TEMPLATE > $DESTDIR/DEBIAN/control
dpkg-deb --build $DESTDIR
mv $PACKAGE .
