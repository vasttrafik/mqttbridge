#FROM debian:stretch
FROM arm32v7/debian:stretch

MAINTAINER Docilitas AB <info@docilitas.se>

RUN apt-get update
RUN apt-get install -y build-essential
RUN apt-get install -y curl ca-certificates apt-utils python2.7 libpython2.7-dev python-virtualenv python-setuptools

COPY mqttbridge /usr/src/mqttbridge/
WORKDIR /usr/src/mqttbridge
RUN virtualenv -p /usr/bin/python2.7 /opt/mqttbridge
RUN /opt/mqttbridge/bin/python setup.py install
RUN rm -fr /opt/mqttbridge/share

COPY debian /usr/src/debian/
WORKDIR /usr/src/debian/
#RUN bash package.sh
