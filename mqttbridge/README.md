# MQTT Bridge

mqttbridge is a bridge between two different MQTT brokers. A typical use is if the original broker is using an old MQTT (v3.1) and we need the 
messages to be published on the standard MQTT (v3.1.1).


## Build requirements

- Python 2.7
- python-virtualenv
- docker and docker-compose (if building .deb-packages)
- qemu-user-static (for cross compiling)
