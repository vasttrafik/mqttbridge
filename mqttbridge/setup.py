#!/usr/bin/env python

from setuptools import setup
from mqttbridge import __version__

setup(
    name='mqttbridge',
    version=__version__,
    description='A MQTT bridge for Bobcat Validator',
    author='Docilitas AB',
    author_email='info@docilitas.se',
    classifiers=['License :: Other/Proprietary License'],
    url='https://bitbucket.org/vasttrafik/mqttbridge',
    packages=[
        'mqttbridge',
    ],
    package_data={'mqttbridge': [
        'schema/*.yaml',
    ]},
    install_requires=[
	'jsonschema==2.6.0',
	'logging==0.4.9.6',
	'paho-mqtt==1.3.1',
	'pyyaml==3.12',
    ],
    data_files=[
        ('examples', ['examples/mqttbridge.yaml']),
    ],
    entry_points={
        "console_scripts": [
            "mqttbridge = mqttbridge.main:main",
        ]
    }
)
