# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: journey.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='journey.proto',
  package='',
  syntax='proto2',
  serialized_pb=_b('\n\rjourney.proto\"\x82\x02\n\rJourneyDigest\x12\x1e\n\x06status\x18\x01 \x02(\x0e\x32\x0e.JourneyStatus\x12\x13\n\x0bjourneyName\x18\x02 \x02(\t\x12\x10\n\x08lineName\x18\x03 \x02(\t\x12\x18\n\x10journeyStartTime\x18\x04 \x02(\r\x12\x13\n\x0b\x64\x65stination\x18\x05 \x02(\t\x12\x0b\n\x03via\x18\x06 \x02(\t\x12\x14\n\x0c\x64\x65stination2\x18\x07 \x01(\t\x12\x0c\n\x04via2\x18\x08 \x01(\t\x12\x11\n\tjourneyId\x18\t \x01(\t\x12\x0e\n\x06lineId\x18\n \x01(\t\x12\x0f\n\x07routeId\x18\x0c \x01(\t\x12\x16\n\x0eticketTariffId\x18\r \x01(\t\"<\n\x0bJourneyInfo\x12\x1e\n\x06\x64igest\x18\x01 \x02(\x0b\x32\x0e.JourneyDigest\x12\r\n\x05stops\x18\x02 \x03(\t\"\x83\x01\n\x04Stop\x12\x10\n\x08\x61reaName\x18\x01 \x01(\t\x12\x16\n\x0eindexInJourney\x18\x02 \x01(\r\x12\x15\n\rstopPointName\x18\x03 \x01(\t\x12\x0e\n\x06\x61reaId\x18\x04 \x01(\t\x12\x13\n\x0bstopPointId\x18\x05 \x01(\t\x12\x15\n\rticketZoneIds\x18\x06 \x03(\t\"\x1c\n\x08Offroute\x12\x10\n\x08offroute\x18\x01 \x02(\x08\"\"\n\x0bStopPressed\x12\x13\n\x0bstopPressed\x18\x01 \x02(\x08\"\x1c\n\x08\x44oorOpen\x12\x10\n\x08\x64oorOpen\x18\x01 \x02(\x08\"\x18\n\x06\x41tStop\x12\x0e\n\x06\x61tStop\x18\x01 \x02(\x08*\x83\x01\n\rJourneyStatus\x12\x17\n\x13JOURNEY_STATUS_NONE\x10\x00\x12#\n\x1fJOURNEY_STATUS_WAITING_TO_START\x10\x01\x12\x1a\n\x16JOURNEY_STATUS_STARTED\x10\x02\x12\x18\n\x14JOURNEY_STATUS_ENDED\x10\x03\x42,\n*net.volvo.vms.vehicle.infohub.mqtt.journey')
)
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

_JOURNEYSTATUS = _descriptor.EnumDescriptor(
  name='JourneyStatus',
  full_name='JourneyStatus',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='JOURNEY_STATUS_NONE', index=0, number=0,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='JOURNEY_STATUS_WAITING_TO_START', index=1, number=1,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='JOURNEY_STATUS_STARTED', index=2, number=2,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='JOURNEY_STATUS_ENDED', index=3, number=3,
      options=None,
      type=None),
  ],
  containing_type=None,
  options=None,
  serialized_start=597,
  serialized_end=728,
)
_sym_db.RegisterEnumDescriptor(_JOURNEYSTATUS)

JourneyStatus = enum_type_wrapper.EnumTypeWrapper(_JOURNEYSTATUS)
JOURNEY_STATUS_NONE = 0
JOURNEY_STATUS_WAITING_TO_START = 1
JOURNEY_STATUS_STARTED = 2
JOURNEY_STATUS_ENDED = 3



_JOURNEYDIGEST = _descriptor.Descriptor(
  name='JourneyDigest',
  full_name='JourneyDigest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='status', full_name='JourneyDigest.status', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='journeyName', full_name='JourneyDigest.journeyName', index=1,
      number=2, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='lineName', full_name='JourneyDigest.lineName', index=2,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='journeyStartTime', full_name='JourneyDigest.journeyStartTime', index=3,
      number=4, type=13, cpp_type=3, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='destination', full_name='JourneyDigest.destination', index=4,
      number=5, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='via', full_name='JourneyDigest.via', index=5,
      number=6, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='destination2', full_name='JourneyDigest.destination2', index=6,
      number=7, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='via2', full_name='JourneyDigest.via2', index=7,
      number=8, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='journeyId', full_name='JourneyDigest.journeyId', index=8,
      number=9, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='lineId', full_name='JourneyDigest.lineId', index=9,
      number=10, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='routeId', full_name='JourneyDigest.routeId', index=10,
      number=12, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='ticketTariffId', full_name='JourneyDigest.ticketTariffId', index=11,
      number=13, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=18,
  serialized_end=276,
)


_JOURNEYINFO = _descriptor.Descriptor(
  name='JourneyInfo',
  full_name='JourneyInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='digest', full_name='JourneyInfo.digest', index=0,
      number=1, type=11, cpp_type=10, label=2,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='stops', full_name='JourneyInfo.stops', index=1,
      number=2, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=278,
  serialized_end=338,
)


_STOP = _descriptor.Descriptor(
  name='Stop',
  full_name='Stop',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='areaName', full_name='Stop.areaName', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='indexInJourney', full_name='Stop.indexInJourney', index=1,
      number=2, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='stopPointName', full_name='Stop.stopPointName', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='areaId', full_name='Stop.areaId', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='stopPointId', full_name='Stop.stopPointId', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='ticketZoneIds', full_name='Stop.ticketZoneIds', index=5,
      number=6, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=341,
  serialized_end=472,
)


_OFFROUTE = _descriptor.Descriptor(
  name='Offroute',
  full_name='Offroute',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='offroute', full_name='Offroute.offroute', index=0,
      number=1, type=8, cpp_type=7, label=2,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=474,
  serialized_end=502,
)


_STOPPRESSED = _descriptor.Descriptor(
  name='StopPressed',
  full_name='StopPressed',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='stopPressed', full_name='StopPressed.stopPressed', index=0,
      number=1, type=8, cpp_type=7, label=2,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=504,
  serialized_end=538,
)


_DOOROPEN = _descriptor.Descriptor(
  name='DoorOpen',
  full_name='DoorOpen',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='doorOpen', full_name='DoorOpen.doorOpen', index=0,
      number=1, type=8, cpp_type=7, label=2,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=540,
  serialized_end=568,
)


_ATSTOP = _descriptor.Descriptor(
  name='AtStop',
  full_name='AtStop',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='atStop', full_name='AtStop.atStop', index=0,
      number=1, type=8, cpp_type=7, label=2,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=570,
  serialized_end=594,
)

_JOURNEYDIGEST.fields_by_name['status'].enum_type = _JOURNEYSTATUS
_JOURNEYINFO.fields_by_name['digest'].message_type = _JOURNEYDIGEST
DESCRIPTOR.message_types_by_name['JourneyDigest'] = _JOURNEYDIGEST
DESCRIPTOR.message_types_by_name['JourneyInfo'] = _JOURNEYINFO
DESCRIPTOR.message_types_by_name['Stop'] = _STOP
DESCRIPTOR.message_types_by_name['Offroute'] = _OFFROUTE
DESCRIPTOR.message_types_by_name['StopPressed'] = _STOPPRESSED
DESCRIPTOR.message_types_by_name['DoorOpen'] = _DOOROPEN
DESCRIPTOR.message_types_by_name['AtStop'] = _ATSTOP
DESCRIPTOR.enum_types_by_name['JourneyStatus'] = _JOURNEYSTATUS

JourneyDigest = _reflection.GeneratedProtocolMessageType('JourneyDigest', (_message.Message,), dict(
  DESCRIPTOR = _JOURNEYDIGEST,
  __module__ = 'journey_pb2'
  # @@protoc_insertion_point(class_scope:JourneyDigest)
  ))
_sym_db.RegisterMessage(JourneyDigest)

JourneyInfo = _reflection.GeneratedProtocolMessageType('JourneyInfo', (_message.Message,), dict(
  DESCRIPTOR = _JOURNEYINFO,
  __module__ = 'journey_pb2'
  # @@protoc_insertion_point(class_scope:JourneyInfo)
  ))
_sym_db.RegisterMessage(JourneyInfo)

Stop = _reflection.GeneratedProtocolMessageType('Stop', (_message.Message,), dict(
  DESCRIPTOR = _STOP,
  __module__ = 'journey_pb2'
  # @@protoc_insertion_point(class_scope:Stop)
  ))
_sym_db.RegisterMessage(Stop)

Offroute = _reflection.GeneratedProtocolMessageType('Offroute', (_message.Message,), dict(
  DESCRIPTOR = _OFFROUTE,
  __module__ = 'journey_pb2'
  # @@protoc_insertion_point(class_scope:Offroute)
  ))
_sym_db.RegisterMessage(Offroute)

StopPressed = _reflection.GeneratedProtocolMessageType('StopPressed', (_message.Message,), dict(
  DESCRIPTOR = _STOPPRESSED,
  __module__ = 'journey_pb2'
  # @@protoc_insertion_point(class_scope:StopPressed)
  ))
_sym_db.RegisterMessage(StopPressed)

DoorOpen = _reflection.GeneratedProtocolMessageType('DoorOpen', (_message.Message,), dict(
  DESCRIPTOR = _DOOROPEN,
  __module__ = 'journey_pb2'
  # @@protoc_insertion_point(class_scope:DoorOpen)
  ))
_sym_db.RegisterMessage(DoorOpen)

AtStop = _reflection.GeneratedProtocolMessageType('AtStop', (_message.Message,), dict(
  DESCRIPTOR = _ATSTOP,
  __module__ = 'journey_pb2'
  # @@protoc_insertion_point(class_scope:AtStop)
  ))
_sym_db.RegisterMessage(AtStop)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), _b('\n*net.volvo.vms.vehicle.infohub.mqtt.journey'))
# @@protoc_insertion_point(module_scope)
