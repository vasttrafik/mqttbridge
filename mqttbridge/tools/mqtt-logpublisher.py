#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import time
import base64
import pdb  # Debugging tool
import paho.mqtt.client as mosquitto

import journey_pb2

mqtt_host = '192.168.66.31'
mqtt_port = 1883
mqtt_protocol = 4

#pdb.set_trace()

NextStop = journey_pb2.Stop()
CurrentStop = journey_pb2.Stop()
Line = journey_pb2.JourneyInfo()
Journey = journey_pb2.JourneyDigest()
AtStop = journey_pb2.AtStop()

publisher = mosquitto.Client('datafeeder', protocol=mqtt_protocol)
publisher.connect(mqtt_host, mqtt_port)
publisher.loop_start()

with open('mqtt-log.txt') as csvfile:
    reader = csv.DictReader(csvfile)

    for row in reader:
        topic = row[' topic'].strip()
        QoS = int(row[' QoS'])
        payload = base64.b64decode(row[' Base64 payload'].strip())
        print(topic
              )
        if topic == '/service/infohub/v1/journey':
            Line.ParseFromString(payload)
            line = Line.digest.lineId
            print('Topic: %s, LineId: %s' % (topic, line))

        elif topic == '/service/infohub/v1/lastStop':
            CurrentStop.ParseFromString(payload)
            print('Topic: %s, StopName: %s, StopId: %s' % (topic, CurrentStop.areaName, CurrentStop.areaId))

        elif topic == '/service/infohub/v1/nextStop':
            NextStop.ParseFromString(payload)
            print('Topic: %s, StopName: %s, StopId: %s' % (topic, NextStop.areaName, NextStop.areaId))

        else:
            print('Topic: %s, payload: %s' % (topic, payload))

        #publisher.publish(topic, payload, QoS)
        publisher.publish(topic, payload, 0)
        time.sleep(1)

