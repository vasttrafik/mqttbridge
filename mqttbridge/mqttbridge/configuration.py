"""Configuration management for mqttbridge"""

import os
import sys
import logging
import jsonschema
import yaml
from mqttbridge.schema_validator import SchemaValidator

DEFAULT_CONF = '/etc/mqttbridge.yaml'

ALL_SCHEMAS = [
    'mqttbridge',
]


class Configuration(object):
    """MQTT Bridge Configuration"""

    def __init__(self, conf, base_dir):

        self.base_dir = base_dir
        self.schemas = SchemaValidator(ALL_SCHEMAS)
        try:
            self.schemas.validate(conf, 'mqttbridge')
        except jsonschema.exceptions.ValidationError as error:
            logging.critical("Syntax error parsing configuration: %s", error.message)
            sys.exit(-1)
        self.listen_to = conf['listen_to']
        self.publish_to = conf['publish_to']
        self.subscribe = conf['subscribe']

    def filename(self, work_file):
        """Add base path to relative file names"""
        return os.path.join(self.base_dir, work_file)

    @classmethod
    def create_from_config_file(cls, filename):
        """Load configuration as YAML"""
        logging.debug("Reading configuration from %s", filename)
        with open(filename, "rt") as work_file:
            config_dict = yaml.load(work_file)
        base_dir = os.path.dirname(filename)
        return cls(config_dict, base_dir)
