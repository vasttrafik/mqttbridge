"""mqttbridge, MQTT listener and publisher"""

import logging
import paho.mqtt.client as mosquitto


class MQTTListener(object):
    """Define and activate the bridge functionality"""

    def __init__(self, config):
        self.listener = mosquitto.Client('mqttbridge', protocol=config.listen_to['parameters']['protocol'])
        self.listener.on_connect = self.on_connect
        self.listener.on_subscribe = self.on_subscribe
        self.listener.on_message = self.on_message
        self.listener.connect(config.listen_to['url'], int(config.listen_to['port']))

        self.publisher = mosquitto.Client('mqttbridge', protocol=config.publish_to['parameters']['protocol'])
        self.publisher.on_connect = self.on_connect_published
        self.publisher.connect(config.publish_to['url'], int(config.publish_to['port']))

        for unit in config.subscribe:
            self.listener.subscribe(unit['topic'], qos=unit['qos'])

        self.publisher.loop_start()
        self.listener.loop_forever()

    def on_connect_published(self, *args):
        """Log when connection is received"""
        rc = args[3]
        logging.debug("Connected to publisher with rc=%s", rc)

    def on_connect(self, *args):
        """Log when connection is received"""
        rc = args[3]
        logging.debug("Connected to listener with rc=%s", rc)

    def on_subscribe(self, *args):
        """Log when subscription is acknowledged"""
        mid = args[2]
        granted_qos = args[3]
        logging.debug("Subscribed to %s with QoS=%s", mid, granted_qos)

    def on_message(self, *args):
        """Publish received message on the new MQTT broker"""
        msg = args[2]
        self.publisher.publish(msg.topic, msg.payload, msg.qos)
        logging.debug("Publish on %s", msg.topic)
