"""mqttbridge main """

import argparse
import logging
from mqttbridge.configuration import Configuration
from mqttbridge.mqtt import MQTTListener


def main():
    """ Main function"""

    parser = argparse.ArgumentParser(description='MQTT Bridge, bridge two MQTT brokers ')

    parser.add_argument('--config',
                        dest='config',
                        metavar='filename',
                        help='mqttbridge configuration file',
                        default='/etc/mqttbridge.yaml')
    parser.add_argument('--debug',
                        dest='debug',
                        action='store_true',
                        help="Enable debugging")

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    config = Configuration.create_from_config_file(args.config)

    MQTTListener(config)


if __name__ == "__main__":
    main()
